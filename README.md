Build the project:
`make`

Create deb-package:
`dpkg --build package`

Install deb-package:
`dpkg -i package.deb`

Usage:
`prime-number <number>`

Remove dev-package:
`dpkg -r prime-number`
